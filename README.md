# README #

This python scipt is designed to take a repository of MFT records and return the Std Info and Filename timestamps as a report.
The program then compares these timestamps against those of other files and returns which files may have associations based on their temporal poximity.

In order to use the program the user must first have extracted MFT records from a suitable hex editor as bytes data (ie copying the 1024 bytes to a directory. 
The program reads in bytes data from files that are in a single directory - the path of which must be given as input to the program.

The user will be given a list of files found before being prompted to continue.
If the user chooses to continue, a report of timestamps will be created in the same path as the MFT records. These times are UTC.

The default time window that the program uses to compare files against each other is half a second. 
After this check has been run the user is informed how many matches have been found and asked if they wish the matches to be appened to the report.
The user is then given the opportunity to specify a user defined window if required and choose if they wish to append these matches to the report.

A log of the program's progress is also created in the same path as the MFT records.

The Script.py is the main program file. The other files within the reporsitory are modules that the Script.py calls on (or superceded modules provided so the evolution can be seen).