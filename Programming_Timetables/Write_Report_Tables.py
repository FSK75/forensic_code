#Function for returning the chosen values from the list of returned timestamp dictionaries (one dictionary per MFT record)
#My timetables object is a list of dictionaries so that I can refer to values elsewhere by their key, ie. MFT Modify Time, Record Name etc.
#However, they can only be written out to a file as a string - hence the requirement to unpack all the attributes and concatenate them to Key:Value pairs.
import os
import logging
def report_readable_times (timetables, user_path):
    write_path = os.path.join(user_path, "report.txt")  #open a file to write report.
    file_pointer = open(write_path, "a")  #append report
    for table in timetables:
        #extract values from the timetables list for each Dictionary (Raw 64 bit integers not required from Dictionary)
        std_info_report = ("Std Info Timestamp Attributes for ", table["Record Name"], "Create -", table["Std Info Create"], "Modify -", table["Std Info Modify"], "MFT Modify -", table["Std Info MFT Modify"], "Accessed -", table["Std Info Accessed"])
        a,b,c,d,e,f,g,h,i,j = std_info_report  #unpack the attribute keys and values.
        file_pointer.write(a+b)
        file_pointer.write("\n")
        file_pointer.write(c+d)
        file_pointer.write("\n")
        file_pointer.write(e+f)
        file_pointer.write("\n")
        file_pointer.write(g+h)
        file_pointer.write("\n")
        file_pointer.write(i+j)
        file_pointer.write("\n")
        file_pointer.write("\n")
        filename_info_report = ("Filename Timestamp Attributes for", table["Record Name"], "Create -", table["Filename Create"], "Modify -", table["Filename Modify"], "MFT Modify -", table["Filename MFT Modify"], "Accessed -", table["Filename Accessed"])
        k,l,m,n,o,p,q,r,s,t = filename_info_report
        file_pointer.write(k+l)
        file_pointer.write("\n")
        file_pointer.write(m+n)
        file_pointer.write("\n")
        file_pointer.write(o+p)
        file_pointer.write("\n")
        file_pointer.write(q+r)
        file_pointer.write("\n")
        file_pointer.write(s+t)
        file_pointer.write("\n")
        file_pointer.write("\n")
        file_pointer.write("\n")
        file_pointer.write("\n")
        logging.basicConfig(filename = os.path.join(user_path, "log.txt"), filemode = "a", level = logging.INFO)
        logging.info("Report written for {} MFT record".format(table["Record Name"]))
    file_pointer.close()

