
#A very painful process for comparing attributes against each other. For just 1 pair that is x 64 comparisons.
#Abandoned for obvious reasons as even copy and paste was taking too long.
#See Comparer 2 which is more compact but requires an additional list of sub lists of timestamps


def compare_to_seconds (timetables, total_files):
    i=0

    while i < total_files -1:
        j=i+1
        print(timetables[i]["Record Name"], "compare with", timetables[j]["Record Name"], ":")
        if timetables[i]['Std Info Create Raw'] <= (timetables[j]['Std Info Create Raw']+1000000000) and timetables[i]['Std Info Create Raw'] >= (timetables[j]['Std Info Create Raw']-1000000000):
            print (timetables[i]["Record Name"], "create time is within 1 second of", timetables[j]["Record Name"], "create time!")
        elif timetables[i]['Std Info Create Raw'] <= (timetables[j]['Std Info Modify Raw'] + 1000000000) and timetables[i]['Std Info Create Raw'] >= (timetables[j]['Std Info Modify Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "create time is within 1 second of", timetables[j]["Record Name"],"modify time!")
        elif timetables[i]['Std Info Create Raw'] <= (timetables[j]['Std Info MFT Modify Raw'] + 1000000000) and timetables[i]['Std Info Create Raw'] >= (timetables[j]['Std Info MFT Modify Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "create time is within 1 second of", timetables[j]["Record Name"],"MFT modify time!")
        elif timetables[i]['Std Info Create Raw'] <= (timetables[j]['Std Info Accessed Raw'] + 1000000000) and timetables[i]['Std Info Create Raw'] >= (timetables[j]['Std Info Accessed Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "create time is within 1 second of", timetables[j]["Record Name"],"accessed time!")

        elif timetables[i]['Std Info Modify Raw'] <= (timetables[j]['Std Info Create Raw']+1000000000) and timetables[i]['Std Info Modify Raw'] >= (timetables[j]['Std Info Create Raw']-1000000000):
            print (timetables[i]["Record Name"], "modify time is within 1 second of", timetables[j]["Record Name"], "create time!")
        elif timetables[i]['Std Info Modify Raw'] <= (timetables[j]['Std Info Modify Raw'] + 1000000000) and timetables[i]['Std Info Modify Raw'] >= (timetables[j]['Std Info Modify Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "modify time is within 1 second of", timetables[j]["Record Name"],"modify time!")
        elif timetables[i]['Std Info Modify Raw'] <= (timetables[j]['Std Info MFT Modify Raw'] + 1000000000) and timetables[i]['Std Info Modify Raw'] >= (timetables[j]['Std Info MFT Modify Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "modify time is within 1 second of", timetables[j]["Record Name"],"MFT modify time!")
        elif timetables[i]['Std Info Modify Raw'] <= (timetables[j]['Std Info Accessed Raw'] + 1000000000) and timetables[i]['Std Info Modify Raw'] >= (timetables[j]['Std Info Accessed Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "modify time is within 1 second of", timetables[j]["Record Name"],"accessed time!")

        elif timetables[i]['Std Info MFT Modify Raw'] <= (timetables[j]['Std Info Create Raw']+1000000000) and timetables[i]['Std Info MFT Modify Raw'] >= (timetables[j]['Std Info Create Raw']-1000000000):
            print (timetables[i]["Record Name"], "MFT modify time is within 1 second of", timetables[j]["Record Name"], "create time!")
        elif timetables[i]['Std Info MFT Modify Raw'] <= (timetables[j]['Std Info Modify Raw'] + 1000000000) and timetables[i]['Std Info MFT Modify Raw'] >= (timetables[j]['Std Info Modify Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "MFT modify time is within 1 second of", timetables[j]["Record Name"],"modify time!")
        elif timetables[i]['Std Info MFT Modify Raw'] <= (timetables[j]['Std Info MFT Modify Raw'] + 1000000000) and timetables[i]['Std Info MFT Modify Raw'] >= (timetables[j]['Std Info MFT Modify Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "MFT modify time is within 1 second of", timetables[j]["Record Name"],"MFT modify time!")
        elif timetables[i]['Std Info MFT Modify Raw'] <= (timetables[j]['Std Info Accessed Raw'] + 1000000000) and timetables[i]['Std Info MFT Modify Raw'] >= (timetables[j]['Std Info Accessed Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "MFT modify time is within 1 second of", timetables[j]["Record Name"],"accessed time!")

        elif timetables[i]['Std Info Accessed Raw'] <= (timetables[j]['Std Info Create Raw']+1000000000) and timetables[i]['Std Info Accessed Raw'] >= (timetables[j]['Std Info Create Raw']-1000000000):
            print (timetables[i]["Record Name"], "access time is within 1 second of", timetables[j]["Record Name"], "create time!")
        elif timetables[i]['Std Info Accessed Raw'] <= (timetables[j]['Std Info Modify Raw'] + 1000000000) and timetables[i]['Std Info Accessed Raw'] >= (timetables[j]['Std Info Modify Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "access time is within 1 second of", timetables[j]["Record Name"],"modify time!")
        elif timetables[i]['Std Info Accessed Raw'] <= (timetables[j]['Std Info MFT Modify Raw'] + 1000000000) and timetables[i]['Std Info Accessed Raw'] >= (timetables[j]['Std Info MFT Modify Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "access time is within 1 second of", timetables[j]["Record Name"],"MFT modify time!")
        elif timetables[i]['Std Info Accessed Raw'] <= (timetables[j]['Std Info Accessed Raw'] + 1000000000) and timetables[i]['Std Info Accessed Raw'] >= (timetables[j]['Std Info Accessed Raw'] - 1000000000):
            print (timetables[i]["Record Name"], "access time is within 1 second of", timetables[j]["Record Name"],"accessed time!")


        i+=1




