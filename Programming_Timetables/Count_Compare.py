#The final version of Comparer now just returns associations at the file level rather than per attribute.
#The same function is also used to count the number of associations to forewarn the user before they ask for a report.
#The variable count_report is use to dictate what is returned from the function - a count of associations or print to report.

import os
import logging

def compare_to_window(contents, raw_times, user_path, window, count_report):


    write_path = os.path.join(user_path, "report.txt")
    file_pointer = open(write_path, "a")
    total_comparisons = 0
    window_word = "blank"

    if window == 500000000:
        window_word = "half a second"
    #elif window == 15000000000: Originally used as default returns but abandone in favour of user defined window.
        #window_word = "fifteen seconds"
    #elif window == 3600000000000:
        #window_word = "an hour"
    else:
        window_word = str(window/1000000000)+" seconds"

    i = 0
    number_of_associations = 0 # incremented each time one of the conditions below is met.

    while i <= len(contents)-2:
        logging.basicConfig(filename=os.path.join(user_path, "log.txt"), filemode="a", level=logging.INFO)
        logging.info("{} Checked for Associations".format(contents[i]))
        #print(i) used for navigating the loop to chekc it cycles correctly.
        k = i+1
        while k <= len(contents)-1:
            #print("\t",k) used for navigating loop
            j = 0
            while j<=7:
                #print("\t\t", Attributes[j]) used for navigating loop
                x = 0
                while x<=7:
                    #print(contents[i],[j],contents[k],[x]) used to eyeball the complete progress of the loop
                    total_comparisons+=1
                    if raw_times[i][j] <= (raw_times[k][x]+ int(window))and raw_times[i][j] >= (raw_times[k][x]- int(window)) and count_report == "report":
                        a, b, c, d, e, f =(contents[i], " has timestamps within ", window_word, " of those associated with ", contents [k], "\n")
                        association = a + b + c + d + e + f
                        #print(contents[i], contents[k])
                        #print(association)
                        file_pointer.write(association)
                        x = 7
                        j = 7
                    elif raw_times[k][x] <= (raw_times[i][j]+ int(window))and raw_times[k][x] >= (raw_times[i][j]- int(window)) and count_report == "report":
                        a, b, c, d, e, f = (contents[i], " has timestamps within ", window_word, " of those associated with ", contents[k], "\n")
                        association = a + b + c + d + e + f
                        #print(association)
                        file_pointer.write(association)
                        x = 7
                        j = 7
                    elif raw_times[i][j] <= (raw_times[k][x] + int(window)) and raw_times[i][j] >= (raw_times[k][x] - int(window)) and count_report == "count":
                        number_of_associations+=1
                        #print(contents[i], contents[k])
                        x = 7
                        j = 7
                    elif raw_times[k][x] <= (raw_times[i][j] + int(window)) and raw_times[k][x] >= (raw_times[i][j] - int(window)) and count_report == "count":
                        number_of_associations+=1
                        #print(contents[i], contents[k])
                        x = 7
                        j = 7

                    x += 1
                j += 1
            k += 1
        i += 1

    file_pointer.write("\n\n")
    file_pointer.close()

    if count_report == "count":
        return number_of_associations
