
#test directory c:\users\frazer.k\desktop\MFT_Records

#import the os module in order to navigate file system
import os
#import struct for unpacking bytes data
import struct
#import module to produce the report of timestanps
import Write_Report_Tables
#import module to count the number of associations that will be returned if a user wants a report and to write report
import Count_Compare
#import sys to allow program to exit if user does not wish to proceed during program running
import sys
#import logging module to write log of progam process
import logging

#function to check that the files supplied are MFT file records base on finding 'file' at the start
def check_file(name):
    absolute_path = os.path.join(user_path, name)
    file_pointer = open(absolute_path, "rb")
    check_entry = file_pointer.read(4)
    logging.basicConfig(filename=os.path.join(user_path, "log.txt"), filemode = "a", level = logging.INFO)
    logging.info("{} checked for type MFT".format(name))
    return check_entry

#function to retun a human readable time from the extracted decimal integer from struct extracted filetime. Credit Hugh Bothwell from stackoverflow - although has been ammened to cope with decimal time.
def getfiletime(extract):
    import datetime
    microseconds = int(extract) / 10
    seconds, microseconds = divmod (microseconds, 1000000)
    days, seconds = divmod(seconds, 86400)

    nowtime = datetime.datetime(1601,1,1)+ datetime.timedelta (days, seconds, microseconds)
    human_time = format(nowtime, '%a, %d %B %Y %H:%M:%S %Z')
    return human_time

#function to unpack the std_info and filename timestamps from the MFT record
def unpack_attributes (name, userPath):
    logging.basicConfig(filename=os.path.join(user_path, "log.txt"), filemode="a", level=logging.INFO)
    logging.info("{} time attributes read".format(name))
    #print(name) testing purposes
    n=0 #pointer value to increment with offsets to evaluate attribute type

    absolute_path = os.path.join(user_path, name)
    file_pointer = open(absolute_path, "rb")
    file_pointer.seek(20) #get the offset to the first attribute
    data = (file_pointer.read(2))
    Offset_to_first_attribute = struct.unpack("1h", data) #tuple [0] contains offset value
    #print (type(Offset_to_first_attribute[0]), Offset_to_first_attribute) #used to check output of struct unpack
    n+=Offset_to_first_attribute[0]

    got_em_both = 0 # used to confirm that both std info and filename info are captured before escaping while loop.

    while got_em_both != 2:
        file_pointer.seek(n)
        attribute_type_data = (file_pointer.read(4))
        attribute_type = struct.unpack("1i", attribute_type_data)#tuple [0] contains attribute type as a dec int. Now use pointers to work way through attributes.


        if attribute_type[0] == 16:
            file_pointer.seek(n+24)# point to timestamps
            std_times_data = (file_pointer.read(32))
            std_times = struct.unpack("4Q", std_times_data)# extract
            #print (std_times) testing purposes
            got_em_both += 1
            file_pointer.seek(n + 4)
            attribute_size_data = (file_pointer.read(4))# find the attribute size
            attribute_size = struct.unpack("1i", attribute_size_data)
            n+=attribute_size[0] # increment the pointer base to the next attribute
        elif attribute_type[0] == 48:
            file_pointer.seek(n + 32)
            filename_times_data = (file_pointer.read(32))
            filename_times = struct.unpack("4Q", filename_times_data)
            #print(filename_times)testing purposes
            got_em_both += 1
            file_pointer.seek(n + 4)
            attribute_size_data = (file_pointer.read(4))
            attribute_size = struct.unpack("1i", attribute_size_data)
            n += attribute_size[0]
        else:
            file_pointer.seek(n + 4)
            attribute_size_data = (file_pointer.read(4))
            attribute_size = struct.unpack("1i", attribute_size_data)
            n += attribute_size[0]

    file_pointer.close()

    raw_times.append(std_times+filename_times)
    timestamps={"Record Name" : name, "Std Info Create Raw" : std_times[0], "Std Info Create" : getfiletime(std_times[0]), "Std Info Modify Raw" : std_times[1], "Std Info Modify": getfiletime(std_times[1]), "Std Info MFT Modify Raw" : std_times[2], "Std Info MFT Modify" : getfiletime(std_times[2]),"Std Info Accessed Raw" : std_times[3], "Std Info Accessed" : getfiletime(std_times[3]), "Filename Create Raw" : filename_times[0], "Filename Create" : getfiletime(filename_times[0]), "Filename Modify Raw" : filename_times[1], "Filename Modify" : getfiletime(filename_times[1]), "Filename MFT Modify Raw" : filename_times[2], "Filename MFT Modify" : getfiletime(filename_times[2]), "Filename Accessed Raw" : filename_times[3], "Filename Accessed" : getfiletime(filename_times[3])}

    return timestamps



#####################################Program Starts########################################


timetables=[] #to contain my dictionary data of attribute/value times for each file.
raw_times=[] #to contain lists of the 64 bit integer timestamps for each file.

earliest_time = 0  #to bracket the time range of all chosen files.
latest_time = 0

user_path = input("Please enter the absolute filepath to your test directory:\t>")
while os.path.isdir(user_path) == False:
    user_path = input("This is not recognised as a directory, please enter a new path:\t>")
    # print(os.path.isdir(user_path)) used to test if user path test was true or false

contents = os.listdir(user_path) #confirm detected files to the user to give them a warm feeling
total_files = len(contents)
print ("Total number of files found =", total_files)

proceed = 0
while proceed != 'y':
    print("The following files were identified in the path given:", contents)
    proceed=input("Do you wish to continue? y or n\n").lower()
    if proceed == "n":
        sys.exit()


#code to deal with the presence of non MFT files in the directory container. It will not check for dead parrots.
for name in os.listdir(user_path):
    if check_file (name) == b'FILE': #run the check_file function to detect that the files are all MFT entries
        #print(name, "is identified as MFT File entry") - used for testing
        timetables.append(unpack_attributes(name, user_path))  # unpack the time stamps from the files
    else:
        print(name, "is not identified as an MFT file.")
        contents.remove(name)# needed to prevent the file being passed to the comparer function as there would be 1 more file than timerecords and the looping won't work


#walk through all times now recorded in raw_times list and compare against earliest and latest known times: overwrite if appropriate
for list in raw_times:
    for timestamp in list:
        if earliest_time == 0:
            earliest_time = timestamp
        elif earliest_time > timestamp:
            earliest_time = timestamp
        elif latest_time == 0:
            latest_time = timestamp
        elif latest_time < timestamp:
            latest_time = timestamp

#Open a report ready for content to be written into and add the date range covered
write_path = os.path.join(user_path, "report.txt")
file_pointer = open(write_path, "a")
file_pointer.write("Report output from Timetables.py\n\n\n")
a,b,c,d,e = ("The date/time range covered by this report is: ", getfiletime(earliest_time), "  to  ", getfiletime(latest_time), "\n\n")
date_range = a+b+c+d+e #need to unpack the tuple to write to report as a single string
file_pointer.write(date_range)
file_pointer.close()

#Supply the timetables container to the Write_Report_Tables module to unpack and write the relevant content
#Supply user path to allow report to be written back to the right place
Write_Report_Tables.report_readable_times(timetables, user_path)

#modules to return associations between files based on their timestamps.
#given that I returned quite a few for only a tiny amount of files, I've pre-warned the user how many they can expect and allow them not to take a report.
print("\nThe number of associations between files where timestamps are within half a second are:",Count_Compare.compare_to_window (contents, raw_times, user_path, 500000000, "count"))
half_second_report = "nil"
while half_second_report != "y":
    half_second_report = input("Do you wish a list of these associations to be added to you report? y or n:\t").lower()
    if half_second_report == 'y':
        Count_Compare.compare_to_window (contents, raw_times, user_path, 500000000, "report")
    elif half_second_report == "n":
        break

#The 2 small scripts below where abandoned in favour of allowing the user to specify their own time window to check against.
#print("\nThe number of associations between files where timestamps are within 15 seconds are:", Count_Compare.compare_to_window(contents, raw_times, user_path, 15000000000, "count"))
#half_second_report = input("Do you wish a list of these associations to be added to you report? y or n:\t")
#if half_second_report == 'y':
    #Count_Compare.compare_to_window(contents, raw_times, user_path, 500000000, "report")


#print("\nThe number of associations between files where timestamps are within 1 hour are:",Count_Compare.compare_to_window(contents, raw_times, user_path, 3600000000000, "count"))
#half_second_report = input("Do you wish a list of these associations to be added to you report? y or n:\t")
#if half_second_report == 'y':
    #Count_Compare.compare_to_window(contents, raw_times, user_path, 3600000000000, "report")

#script to allow the user to enter their own window to check against if required
user_defined_report_required = "nil"
while user_defined_report_required != "y":
    user_defined_report_required = input ("\nDo you wish to specify a user defined time window to check timestamp associations against? y or n?\t").lower()
    if user_defined_report_required == "y":
        window = int(input("\nHow big do you wish the association window to be? Please enter the window size in seconds.\t"))
        print("\nThe number of associations between files where timestamps are within the user defined period are:",Count_Compare.compare_to_window(contents, raw_times, user_path, window*1000000000, "count"))

        user_defined_report = input("Do you wish a list of these associations to be added to you report? y or n:\t").lower()
        if user_defined_report == 'y':
            Count_Compare.compare_to_window(contents, raw_times, user_path, window*1000000000, "report")
    elif user_defined_report_required == "n":
        break

print ("\nYour report has been returned to your directory at the path", user_path)#finalised report in user directory: now pining for the fjords.

#c:\users\frazer.k\desktop\MFT_Records