#Standalone moule for testing the time extact with known values taken using winhex
def getfiletime(extract):
    import datetime
    #microseconds = int(extract, 16)/10
    microseconds = int(extract) / 10
    seconds, microseconds = divmod (microseconds, 1000000)
    days, seconds = divmod(seconds, 86400)

    nowtime = datetime.datetime(1601,1,1)+ datetime.timedelta (days, seconds, microseconds)
    human_time = format(nowtime, '%a, %d %B %Y %H:%M:%S %Z')
    return human_time

#print (format(getfiletime('131218588391890205'), '%a, %d %B %Y %H:%M:%S %Z'))
print (getfiletime('131218588391890205'))

#print(int('01d20c3848a3b937', 16))

#output in dec = 131180775023556919
#output from struct =